#!/bin/bash

# Please define needed vars before calling this script


main() {
	mkdir -p ${INST_PATH}
	do_kernel
	do_dtb
}

function do_kernel() {
	kernel_fetch
	cd ${KERNEL_BUILD_DIR}
	kernel_patch
	kernel_configure
	kernel_compile
	cd -
}

function do_dtb() {
	echo "Creating dtb..."
	# ${DTC_PATH} -O dtb -o ${DTB_FILE} ${DTS_FILE}
	cp ${DTB_FILE} ${INST_PATH}
}



function kernel_patch() {
	echo "Patching kernel..."
	# nothing
	
}


function kernel_fetch() {
	echo "Fetching kernel..."
	
	if [ -d "$KERNEL_BUILD_DIR" ]; then
		echo "Kernel directory already exists, using it..."
	else
		git clone ${KERNEL_GIT_SRC} ${KERNEL_BUILD_DIR}
		cd ${KERNEL_BUILD_DIR}
		git checkout ${KERNEL_GIT_BRANCH} -b local/${KERNEL_GIT_BRANCH}
		cd -
	fi
}

function kernel_configure() {

	if [[ -e ".config" ]]
	then
		echo
		echo "A .config file already exists in linux source directory."
		read -p "Do you want to restore the default kernel configuration (your modifications will be lost) ?  [y/n]: " -r
		echo
		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
			return
		fi
	fi
	
		
	if [[ -e ".config" ]]
	then
		echo
		echo "A .config file already exists in linux source directory."
		read -p "Do you want to restore the default kernel configuration (your modifications will be lost) ?  [y/n]: " -r
		echo
		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
			return
		fi
	fi
	


}


function kernel_compile() {
	echo "Compiling kernel..."
	make -j${BUILD_NB_THREADS}
	make -j${BUILD_NB_THREADS} modules
	make modules_install INSTALL_MOD_PATH=../${MODULES_INST_PATH}
	cp arch/arm/boot/zImage ../${INST_PATH}
}
